package com.ninad.springcloud.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigClientController {
	
	@Value("${user.role}")
	private String role;

	@Value("${spring.profiles.active}")
	private String profile;

	@Autowired
	private ClientAppConfiguration clientAppConfiguration;

	@RequestMapping(value = "/hello/{username}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public String sayHello(@PathVariable("username") String username) {
		System.out.println("===> " + clientAppConfiguration.getRole());
		return String.format("Hello <b>%s</b>. <br>Profile is <b>%s</b>.<br>Your role is <b>%s</b>.<br>Role from ClientAppConfiguration is <b>%s</b>.",
				username, profile, role, clientAppConfiguration.getRole());
	}
}
