http://tech.asimio.net/2017/02/02/Refreshable-Configuration-using-Spring-Cloud-Config-Server-Spring-Cloud-Bus-RabbitMQ-and-Git.html

CREATING GIT CONFIG REPO
===============================================================================================================
mkdir config-repo
cd config-repo
git init .
echo user.role=Developer > config-client-development.properties
echo user.role=User > config-client-production.properties
git add -A .
git commit -m "Add config-client-development"

===============================================================================================================
WITH DEVELOPMENT PROFILE
===============================================================================================================
1. Start config server.
1.a. Hit below config url to see whether configs are returned by config server 
http://localhost:8888/config-client/development/
http://localhost:8888/config-client/production/
http://localhost:8888/config-client-production.yml
http://localhost:8888/config-client-production.properties

# ConfigServer endpoints to read configuration properties:
#/{application}/{profile}[/{label}]
#/{application}-{profile}.yml
#/{label}/{application}-{profile}.yml
#/{application}-{profile}.properties
#/{label}/{application}-{profile}.properties

2. Start config client with development profile. Pass JVM args -Dspring.profiles.active=development or change spring.profiles.active=development in application.proprties
3. http://localhost:8080/hello/Ninad
	Hello Ninad. 
	Profile is development.
	Your role is Developer User.
	Role from ClientAppConfiguration is Developer User.
4. Change the value of below property in config-client-development.properties 
----------------------------------------------------------------------------
echo user.role=Developer Changed > config-client-development.properties
git add -A .
git commit -m "Add config-client-development"	
----------------------------------------------------------------------------
5. Send a post request from the config client using postman to below url
http://localhost:8080/refresh
6. Hit the hello url and you will see the property changing without refresh.
http://localhost:8080/hello/Ninad
Hello! You're Ninad and you'll become a(n) Developer Changed...
Role from ClientAppConfiguration = Developer Changed

===============================================================================================================	
WITH PRODUCTION PROFILE
===============================================================================================================
1. Start config server.
2. Start config client with development profile. Pass JVM args -Dspring.profiles.active=production or change spring.profiles.active=production in application.proprties
3. http://localhost:8080/hello/Ninad
	Hello Ninad. 
	Profile is production.
	Your role is Production User.
	Role from ClientAppConfiguration is Production User.
#4,5,6 can be done for production properties file




=======================
Send POST request using postman to simulate webhook
	http://localhost:8888/monitor

Below are headers
	Content-Type:application/json
	X-Event-Key:repo:push
	X-Hook-UUID: webhook-uuid

Below is the raw body
	{"push": {"changes": []}}

=======================
